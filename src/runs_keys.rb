
module Runs


  class Keys

    VERSION = '1.0.0'

    LANGUAGE_CATEGORY = 'Objc'

    FILE_NAME = ''
    FILE_PATH = ''

    PROJECT_NAME = ''
    PROJECT_PATH = ''

    SHOULD_ADD_REF_TO_PROJ = false

    AUTHOR_NAME = 'Ruby'
    ORGANIZATION_NAME = 'Runs.com'

    SUPER_CLASS_NAME = 'NSObject'

    SYS_HEADERS = []
    OTHER_CLASS_HEADERS = []

    CLASS_ARRAY = []
    CLASS_PROPERTY_HASH = []

  end

end
