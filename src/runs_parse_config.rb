require '../src/runs_keys'

module Runs

  class ParseConfig

    def initialize(file_path)
      puts "file ptah : #{file_path}"

      if !File.exist?(file_path)
        puts "File is not exist. Exit"
        exit
      end
      config_hash = parse_to_hash(file_path)
      config_hash
    end

    def parse_to_hash(file_path)
        parse_head_config(file_path)
        parse_struct_property(file_path)
    end


    def parse_struct_property(file_path)

    end

    # 解析每一行的参数配置
    # 只是解析包含‘=’的每一行数据
    # 解析原则：
    #   1. 首先通过‘=’拆分,‘=’前面的字符串为key 后面的为value(不包含‘#’号后面的字符串)
    #   2. 检查value是否包含‘#’，如果有进行‘#’分割 ，取‘#’前面的字符串
    #   3. 移除字符串前后的空格

    def parse_head_config(file_path)
      @head_key_values = {}
      File.open(file_path,"r+") { |file|
        file.each_line { |line|
          # puts line
          next if line.to_s.start_with?('#')
          next if !line.to_s.include?('=')
          arr = line.to_s.split('=')
          arr.map { |obj|
            obj.delete!(' ')
            obj.nil? ? obj : obj.chomp!

            if obj.to_s.include?('#')
              array = obj.to_s.split('#')
              arr.delete(obj)
              arr.push(array.first)
            end

          }.reject! { |obj1|
            obj1.to_s.empty? || obj1.nil?
          }
          next if arr.nil? || arr.size < 2
          @head_key_values[arr.first] = arr.last
        }
      }

      @head_key_values.keys.each { |key|
        # puts "key len : #{key.to_s.length}"
        # puts "value len : #{@key_values[key].to_s.length}"
        puts "#{key} : #{@head_key_values[key]}"
      }
    end


  end
end

# RunsParseConfig.new