require 'optparse'
require 'pathname'
require '../src/runs_keys'
require '../src/runs_parse_config'

module Runs

  class Generate

    def initialize(file_path)
      parse_config(file_path)
    end

    def parse_config(file_path)
      @configs = ParseConfig.new(file_path)
    end

  end


  def backtrace_for_all_threads(signame)

    if Thread.current.respond_to?(:backtrace)
      Thread.list.each do |t|
        puts t.backtrace
      end
    else
      puts caller
    end
  end

  module_function :backtrace_for_all_threads

end


# options = {}
OptionParser.new do |opts|
  opts.banner = "Here is help messages of the command line tool.\nUsage: example.rb [options]"

  opts.on('-v', '--version', 'show version NO.') do
    # options['Version'] = Runs::Keys::VERSION
    puts "Version : #{Runs::Keys::VERSION}"
  end

  opts.on('-m Message','--message','input some message') do |value|
    # options['Message'] = value
  end

  opts.on('-f File Path','--fliepath','input some fliepath') do |value|
    # options['File Path'] = value
    Runs::Generate.new(value)
  end

end.parse!

# options.keys.each { |key|
#   puts "#{key} : #{options[key]}"
# }
