module Runs

  module Property

    ASSIGN_PROPERTY_PREFIX  = '@property (nonatomic, assign)'
    COPY_PROPERTY_PREFIX    = '@property (nonatomic, copy)'
    STRONG_PROPERTY_PREFIX  = '@property (nonatomic, strong)'
    WEAK_PROPERTY_PREFIX    = '@property (nonatomic, weak)'


    def current_year
      time = Time.new
      time.year
    end

    def format_time
      time = Time.new
      time.strftime("%Y/%m/%d")
    end

  end


  class PropertyModel
    attr_accessor :class_name,
                  :file_name,
                  :objc_sys_headers,
                  :swift_sys_headers,
                  :other_class_headers,
                  :super_class_name,
                  :property_count,
                  :assign_property_map,
                  :copy_property_map,
                  :strong_property_map,
                  :weak_property_map,
                  :author_name,
                  :organization_name,
                  :project_name

    include Property

    def initialize

    end

    def top_description_for_head_file
        top_description_weith_file_type_name('h')
    end

    def top_description_for_Implementation_file
        top_description_weith_file_type_name('m')
    end

    def top_description_weith_file_type_name(name)
      "//\n//  #{file_name}.#{name}\n//  #{project_name}\n//\n//  Created by #{author_name} on #{format_time}.\n//  Copyright © #{current_year}年 #{organization_name}. All rights reserved.\n//\n//  Auto generate by Ruby script from RunsCode/AutoGenerate\n//  Github:https://github.com/RunsCode/AutoGenerateCode.git\n"
    end

  end



end


# test
property = Runs::PropertyModel.new
property.file_name = 'AppDelegate'
property.project_name = 'Hello World'
property.author_name = 'Runs'
property.organization_name = 'ABC.com'


puts property.top_description_for_head_file

puts '-'*60

puts property.top_description_for_Implementation_file