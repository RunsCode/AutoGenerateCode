
## Auto generate code

> 一款命令行数据模型代码生成器, 他通过读取配置文件config.gen自动生成对应的代码

#### 1、生成`Object-C`代码

###### 1.1、代码主要组成结构
* 模型.h文件  
  > * **Header 说明**      
    |—— //  
    |—— //  类名xxxx.h  
    |—— //  工程名 xxx  
    |—— //  
    |—— //  Created by who on xxxx/xx/xx.  
    |—— //  Copyright © xxxx年 组织名. All rights reserved.  
    |—— //  Generated from Auto Generate(v1.0.0), Created by runs.   
    |—— //  
    |
  > * **头文件区域**  
    |—— 必要头文件 #import <Foundation/Foundation.h>  
    |—— 自定义类引用头文件（不建议 建议使用@class XXX)   
    |   
  > * **代码核心区域 H0** _可重复声明几个模型_  
    |—— @interface xxxx : NSObject  
    |—— 以下至@end为属性声明区域  
    |—— @property (nonatomic, xxxxx, xxxx) XXXX *xxx;  
    |—— @property (nonatomic, xxxxx, xxxx) XXXX *xxx;  
    |—— @end  
    |  
  > * **代码核心区域 H1** 
  > * **代码核心区域 H2**
  > * **代码核心区域 H3** 
    
    
  * 模型.m文件  
    > * **Header 说明**      
      |—— //  
      |—— //  类名xxxx.m  
      |—— //  工程名 xxx  
      |—— //  
      |—— //  Created by who on xxxx/xx/xx.  
      |—— //  Copyright © xxxx年 组织名. All rights reserved.  
      |—— //  Generated from Auto Generate(v1.0.0), Created by runs.   
      |—— //  
      | 
    > * **头文件区域**  
      |—— 必要头文件 #import<XXXX.h>
      |     
    > * **代码核心区域 M0** *可重复声明几个模型*  
      |—— @implementation xxxx  
      |—— @end  
      |  
    > * **代码核心区域 M1** 
    > * **代码核心区域 M2** 
    > * **代码核心区域 M3** 


#### 2、生成`Swift`代码
###### 2.1、结构体值类型 `Struct` 模型
###### 2.2、常规引用类型 `Class` 模型

#### 3、配置文件config.gen 配置结构

###### 3.1、公共参数配置
*按行读取配置，无需分号，逗号之类的分割符号*
```
#语言
LANGUAGE_CATEGORY = xxxx # Objc/Swift.strust or Swift.class
#
# 文件名
# 用头文件说明 比如：User -> User.h 、User.m 
FILE_NAME = xxxx  
#
# 工程名 工程的名字
PROJECT_NAME = xxxx
#
# 工程文件PROJECT_NAME.xcodeproj上层的绝对路径 完整绝对路径为：PROJECT_PATH/PROJECT_NAME.xcodeproj
PROJECT_PATH =  xx/xx
#
# 是否需要自动添加到工程中 默认是0 ，0表示不需要 1：表示需要 ，不需要的情况下FILE_PATH可不填否则必填
# SHOULD_ADD_REF_TO_PROJ = 0
#
# optional 文件需要添加工程中的相对路径 完整绝对路径为：PROJECT_PATH/FILE_PATH
FILE_PATH = xx/xx
```

###### 3.2、Header 文件说明 区域配置

```
# 创建者的名字
AUTHOR_NAME = xxx
#
# 组织的名字
ORGANIZATION_NAME = xxx
```

###### 3.3、头文件 区域配置

```
# optional 可选项
SUPER_CLASS_NAME = xxxx # default is NSObject
#
# optional 系统头文件 默认包含 <Foundation/Foundation.h> 
# A/B/C必须是完整的头文件说明 比如
# A => <UIKit/UIKit.h>
SYS_HEADERS = [A,B,C]
#
# optional 其他自定义头文件 比如
# #import "A.h" OR @class A, B;
# #import "B.h"
OTHER_CLASS_HEADERS = [A,B,C]
```

###### 3.4、核心代码 区域配置
```
<CLASS_A>
BOOL = [] # default readwrite
CGFloat = [] # default readwrite
NSInteger = [] # defaul readwrite
NSUInteger = [] # default readwrite
NSString = [] # default readwrite copy
NSArray = [] # defatult readwrite copy
Custom_A = [] #  defatult readwrite strong
Custom_B = [] #  defatult readwrite strong
</CLASS_A>
#
#optional
#
<CLASS_B>
BOOL = [] # default readwrite
CGFloat = [] # default readwrite
NSInteger = [] # defaul readwrite
NSUInteger = [] # default readwrite
NSString = [] # default readwrite copy
NSArray = [] # defatult readwrite copy
Custom_A = [] #  defatult readwrite strong
Custom_B = [] #  defatult readwrite strong
</CLASS_B>
```
















 


































